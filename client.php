<?php 
ini_set("soap.wsdl_cache_enabled", "0");
$client = new SoapClient(
    "summary.wsdl",
    array('soap_version' => SOAP_1_2, 'trace' => 1));
echo("Dumping client object functions:\n");
var_dump($client->__getFunctions());
echo "\n";

$return = $client->summary("sadas", "sdass");
echo "Returning value: " . $return . "\n";
echo "\n";

// echo "Dumping request headers:\n" . $client->__getLastRequestHeaders() . "\n" ;
// echo "Dumping request:\n" . $client->__getLastRequest() . "\n"; 
// echo "Dumping response headers:\n" . $client->__getLastResponseHeaders() . "\n";
// echo "Dumping response:\n" . $client->__getLastResponse() . "\n";
?>