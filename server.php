<?php 

function summary($a, $b) {
	return $a + $b;
}


ini_set("soap.wsdl_cache_enabled", "0"); 
$server = new SoapServer(
    "summary.wsdl",
    array('soap_version' => SOAP_1_2, 'trace' => 1));
$server->addFunction("summary");
$server->handle(); 
?>
